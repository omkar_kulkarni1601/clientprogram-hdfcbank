package com.client.hdfc.entity;

public class LoanOfferSuppressionRequestDTO {
    private String productCode;
    private String customerId;
    private String action;

    // Getters and setters for productCode, customerId, and action

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
