package com.client.hdfc.entity;

public class MyDataModel {
    private SessionContext sessionContext;
    private LoanOfferSuppressionRequestDTO loanOfferSuppressionRequestDTO;

    // Getters and setters for sessionContext and loanOfferSuppressionRequestDTO

    public SessionContext getSessionContext() {
        return sessionContext;
    }

    public void setSessionContext(SessionContext sessionContext) {
        this.sessionContext = sessionContext;
    }

    public LoanOfferSuppressionRequestDTO getLoanOfferSuppressionRequestDTO() {
        return loanOfferSuppressionRequestDTO;
    }

    public void setLoanOfferSuppressionRequestDTO(LoanOfferSuppressionRequestDTO loanOfferSuppressionRequestDTO) {
        this.loanOfferSuppressionRequestDTO = loanOfferSuppressionRequestDTO;
    }
}

