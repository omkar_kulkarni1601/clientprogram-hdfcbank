package com.client.hdfc.entity;

public class EncryptedDataObject {
    private String requestSignatureEncryptedValue;
    private String symmetricKeyEncryptedValue;
    private String scope;
    private String transactionId;
    private String oAuthTokenValue;

    // Constructors, getters, and setters

    public EncryptedDataObject() {
    }

    public EncryptedDataObject(String requestSignatureEncryptedValue, String symmetricKeyEncryptedValue,
                               String scope, String transactionId, String oAuthTokenValue) {
        this.requestSignatureEncryptedValue = requestSignatureEncryptedValue;
        this.symmetricKeyEncryptedValue = symmetricKeyEncryptedValue;
        this.scope = scope;
        this.transactionId = transactionId;
        this.oAuthTokenValue = oAuthTokenValue;
    }

    public String getRequestSignatureEncryptedValue() {
        return requestSignatureEncryptedValue;
    }

    public void setRequestSignatureEncryptedValue(String requestSignatureEncryptedValue) {
        this.requestSignatureEncryptedValue = requestSignatureEncryptedValue;
    }

    public String getSymmetricKeyEncryptedValue() {
        return symmetricKeyEncryptedValue;
    }

    public void setSymmetricKeyEncryptedValue(String symmetricKeyEncryptedValue) {
        this.symmetricKeyEncryptedValue = symmetricKeyEncryptedValue;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOAuthTokenValue() {
        return oAuthTokenValue;
    }

    public void setOAuthTokenValue(String oAuthTokenValue) {
        this.oAuthTokenValue = oAuthTokenValue;
    }
}
