package com.client.hdfc.controller;

import com.client.hdfc.entity.EncryptedDataObject;
import com.client.hdfc.entity.MyDataModel;
import com.client.hdfc.service.EncryptionService;
import com.client.hdfc.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class MyController {
    private final EncryptionService encryptionService;


    @Autowired
    private OfferService offerService;

//    private final WebClient.Builder webClientBuilder;
//
//    @Autowired
//    public MyController(WebClient.Builder webClientBuilder) {
//        this.webClientBuilder = webClientBuilder;
//    }
//
//    @PostMapping("/post")
//    public Mono<MyDataModel> sendRequest(@RequestBody MyDataModel myDataModel) {
//        String apiUrl = "https://api-uat.hdfcbank.com/enc_dec_util_high"; // Replace with your API endpoint
//
//        return webClientBuilder.build()
//                .post()
//                .uri(apiUrl)
//                .bodyValue(myDataModel)
//                .retrieve()
//                .bodyToMono(MyDataModel.class);
//    }

    @PostMapping("/posting")
    public MyDataModel post(@RequestBody MyDataModel myDataModel) {
        return offerService.postreq(myDataModel);
    }


    public MyController(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @PostMapping("/encrypt-response")
    public EncryptedDataObject encryptResponse(@RequestBody EncryptedDataObject encryptedDataObject) {
//        try {
//            String encryptedResponse = encryptionService.encrypt(responseBody);
//            return ResponseEntity.ok(encryptedResponse);
//        } catch (Exception e) {
//            return ResponseEntity.status(500).body("Error encrypting response: " + e.getMessage());
//        }
//    }
        return offerService.post(encryptedDataObject);
    }
}
