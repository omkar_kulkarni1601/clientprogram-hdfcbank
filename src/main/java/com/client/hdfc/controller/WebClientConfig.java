//package com.client.hdfc.controller;
//
//import org.apache.http.ssl.SSLContextBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.client.reactive.ClientHttpConnector;
//import org.springframework.http.client.reactive.ReactorClientHttpConnector;
//import org.springframework.web.reactive.function.client.WebClient;
//import reactor.netty.http.client.HttpClient;
//import reactor.netty.tcp.SslProvider;
//
//import javax.net.ssl.SSLContext;
//import java.io.FileInputStream;
//import java.security.KeyStore;
//
//@Configuration
//public class WebClientConfig {
//
//    @Bean
//    public WebClient.Builder webClientBuilder() throws Exception {
//        KeyStore keyStore = KeyStore.getInstance("PKCS12");
//        keyStore.load(new FileInputStream("C:\\Users\\omkar\\Downloads\\hdfc Task\\hdfc-Task\\src\\main\\resources\\hdfcbank_uatapitesting.p12"), "7layer".toCharArray());
//
//        SSLContext sslContext = getSslContext(keyStore);
//
//        HttpClient httpClient = HttpClient.create()
//                .secure(sslSpec -> sslSpec.sslContext((SslProvider.ProtocolSslContextSpec) sslContext));
//
//        ClientHttpConnector httpConnector = new ReactorClientHttpConnector(httpClient);
//
//        return WebClient.builder()
//                .clientConnector(httpConnector);
//    }
//
//    private SSLContext getSslContext(KeyStore keyStore) throws Exception {
//        return SSLContextBuilder
//                .create()
//                .loadKeyMaterial(keyStore, "7layer".toCharArray())
//                // You can add more SSL configuration here if needed
//                .build();
//    }
//}
