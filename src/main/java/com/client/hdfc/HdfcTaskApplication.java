package com.client.hdfc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HdfcTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(HdfcTaskApplication.class, args);
	}


}
