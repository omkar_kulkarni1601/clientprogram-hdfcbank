package com.client.hdfc.service;

import com.client.hdfc.entity.EncryptedDataObject;
import com.client.hdfc.entity.MyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OfferServiceImp implements OfferService {
    Logger logger = LoggerFactory.getLogger(OfferServiceImp.class);

    @Autowired
    private RestTemplate restTemplate;

    public  OfferServiceImp(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }
    @Override
    public MyDataModel postreq(MyDataModel myDataModel) {
        MyDataModel response = restTemplate.postForObject("https://api-uat.hdfcbank.com/enc_dec_util_high",myDataModel, MyDataModel.class);
        return response;
    }

    @Override
    public EncryptedDataObject post(EncryptedDataObject encryptedDataObject) {
        EncryptedDataObject response = restTemplate.postForObject("https://api-uat.hdfcbank.com/enc_dec_util_high",encryptedDataObject, EncryptedDataObject.class);
        return response;
    }
}
