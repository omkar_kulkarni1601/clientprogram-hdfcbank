package com.client.hdfc.service;

import com.client.hdfc.entity.EncryptedDataObject;
import com.client.hdfc.entity.MyDataModel;

public interface OfferService {

    MyDataModel postreq(MyDataModel myDataModel);
    EncryptedDataObject post(EncryptedDataObject encryptedDataObject);
}
